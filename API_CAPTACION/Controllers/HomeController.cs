﻿using API_CAPTACION.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;

namespace API_CAPTACION.Controllers
{
    public class HomeController : Controller
    {
        Entities db = new Entities();
        //
        // GET: /Home/

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult Index(FormCollection collection)
        {
            try
            {
                string RUT = "";
                decimal TELEFONO = 0;
                string NOMBRE = "";
                string APELLIDO = "";
                string EMAIL = "";
                DateTime FECHA_NACIMIENTO = DateTime.Now;
                string CODIGO_DSCTO = "";
                string COD_CAMPANA = "";
                string RUT_PERSONA_REGISTRA = "";
                string NOMBRE_PERSONA_REGISTRA = "";
                string TIPO_CAPTACION = "";
                string pais = "CL";
                DateTime FECHA_REGISTRO;

                bool validation = true;
                List<string> mensajes = new List<string>();

                if (collection["pais"] != null)
                {
                    pais = collection["pais"].ToUpper();
                }

                if (collection["rut"] != null)
                {
                    if (Validacion.ValidaRut(collection["rut"]))
                    {
                        RUT = collection["rut"];
                    }
                    else
                    {
                        mensajes.Add("Rut ingresado no es válido");
                        validation = false;
                    }
                }
                else
                {
                    mensajes.Add("El rut es obligatorio");
                    validation = false;
                }

                if (collection["telefono"] != null)
                {

                    if (!Validacion.telefono(collection["telefono"]))
                    {
                        mensajes.Add("Teléfono es obligatorio y debe tener 9 dígitos");
                        validation = false;
                    }
                    else
                    {
                        TELEFONO = decimal.Parse(collection["telefono"]);
                    }
                }
                else
                {
                    mensajes.Add("Teléfono es obligatorio y debe tener 9 dígitos");
                    validation = false;
                }

                if (collection["email"] != null)
                {
                    if (!Validacion.email(collection["email"]))
                    {
                        mensajes.Add("Email ingresado no es válido");
                        validation = false;
                    }
                    EMAIL = collection["email"];
                }

                if (collection["apellido"] != null)
                {
                    APELLIDO = collection["apellido"];
                    if (APELLIDO.Length < 3)
                    {
                        mensajes.Add("Apellido debe tener a lo menos 3 caracteres");
                        validation = false;
                    }
                }

                if (collection["nombre"] != null)
                {
                    NOMBRE = collection["nombre"];
                    if (NOMBRE.Length < 3)
                    {
                        mensajes.Add("Nombre debe tener a lo menos 3 caracteres");
                        validation = false;
                    }
                }

                /*
                if (collection["fecha_nacimiento"] != null)
                {
                    FECHA_NACIMIENTO = DateTime.Parse(collection["fecha_nacimiento"]);
                }*/

                if (collection["rut_persona_registra"] != null)
                {
                    RUT_PERSONA_REGISTRA = collection["rut_persona_registra"];

                    if (RUT_PERSONA_REGISTRA.Length < 3)
                    {
                        mensajes.Add("Código de captador(a) debe tener a lo menos 3 dígitos");
                        validation = false;
                    }

                }
                else
                {
                    mensajes.Add("Ingrese rut de captador(a)");
                    validation = false;
                }

                if (collection["nombre_persona_registra"] != null)
                {
                    NOMBRE_PERSONA_REGISTRA = collection["nombre_persona_registra"];
                    if (NOMBRE_PERSONA_REGISTRA.Length < 3)
                    {
                        mensajes.Add("Nombre debe captador(a) debe tener a lo menos 3 caracteres");
                        validation = false;
                    }
                }
                else
                {
                    mensajes.Add("Ingrese código de captador");
                    validation = false;
                }

                if (collection["tipo_captacion"] != null)
                {
                    TIPO_CAPTACION = collection["tipo_captacion"];
                    if (TIPO_CAPTACION.Length < 3)
                    {
                        mensajes.Add("Tipo captación debe tener a lo menos 3 caracteres");
                        validation = false;
                    }
                }
                else
                {
                    mensajes.Add("Ingrese campaña de captación");
                    validation = false;
                }

                if (collection["codigo_dscto"] != null)
                {
                    CODIGO_DSCTO = collection["codigo_dscto"];
                    if (CODIGO_DSCTO.Length < 3)
                    {
                        mensajes.Add("Código descuento debe tener a lo menos 3 caracteres");
                        validation = false;
                    }
                }
                else
                {
                    mensajes.Add("Ingrese código de descuento");
                    validation = false;
                }

                if (collection["codigo_campana"] != null)
                {
                    COD_CAMPANA = collection["codigo_campana"];
                    if (COD_CAMPANA.Length < 3)
                    {
                        mensajes.Add("Código de campaña debe tener a lo menos 3 caracteres");
                        validation = false;
                    }
                }
                else
                {
                    mensajes.Add("Ingrese código de campaña");
                    validation = false;
                }
                FECHA_REGISTRO = DateTime.Now;

                PROSPECTO_CLIENTES search = (from x in db.PROSPECTO_CLIENTES
                                             where x.TELEFONO == TELEFONO && x.RUT == RUT && x.COD_CAMPANA == COD_CAMPANA
                                             select x).FirstOrDefault();


                var jsonResult = new { Status = "0", Message = "Ya existe registrado el cliente en la campaña", Errores = mensajes };
                if (search == null && validation)
                {
                    int ID = db.Database.SqlQuery<int>("SELECT PK_PROSPECTOS.NEXTVAL FROM dual").Single();
                    PROSPECTO_CLIENTES data = new PROSPECTO_CLIENTES();

                    data.PK_PROSPECTO = ID;
                    data.RUT = RUT;
                    data.TELEFONO = TELEFONO;
                    data.NOMBRE = NOMBRE;
                    data.APELLIDO = APELLIDO;
                    data.EMAIL = EMAIL;
                    data.FECHA_NACIMIENTO = DateTime.Now;
                    data.CODIGO_DSCTO = CODIGO_DSCTO;
                    data.COD_CAMPANA = COD_CAMPANA;
                    data.RUT_PERSONA_REGISTRA = RUT_PERSONA_REGISTRA;
                    data.NOMBRE_PERSONA_REGISTRA = NOMBRE_PERSONA_REGISTRA;
                    data.TIPO_CAPTACION = TIPO_CAPTACION;
                    data.FECHA_REGISTRO = FECHA_REGISTRO;
                    data.PAIS = pais;

                    db.PROSPECTO_CLIENTES.Add(data);
                    db.SaveChanges();

                    jsonResult = new { Status = "1", Message = "OK", Errores = mensajes };
                }



                if (!validation)
                {
                    jsonResult = new { Status = "0", Message = "Error en la creación de captación, verifique los mensajes de error", Errores = mensajes };
                }

                //parametros;
                string log_parametros = "";
                foreach (var key in collection.AllKeys)
                {
                    log_parametros += "|" + key + ":" + collection[key];
                }


                WriteLog(jsonResult.Message + " - " + string.Join(",", jsonResult.Errores.ToArray()) + " | Parametros => " + log_parametros + " \n ");
                return Json(jsonResult, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var jsonResult = new { Status = "0", Message = ex.Message };

                string log_parametros = "";
                foreach (var key in collection.AllKeys)
                {
                    log_parametros += "|" + key + ":" + collection[key];
                }

                WriteLog(jsonResult.Message + "| => linea: " + GetLineNumber(ex).ToString() + " | Parametros => " + log_parametros + " \n ");
                return Json(jsonResult, JsonRequestBehavior.AllowGet);
            }
        }

        public int GetLineNumber(Exception ex)
        {
            var lineNumber = 0;
            const string lineSearch = ":line ";
            var index = ex.StackTrace.LastIndexOf(lineSearch);
            if (index != -1)
            {
                var lineNumberText = ex.StackTrace.Substring(index + lineSearch.Length);
                if (int.TryParse(lineNumberText, out lineNumber))
                {
                }
            }
            return lineNumber;
        }

        public static void WriteLog(string strLog)
        {

            string logFilePath = HostingEnvironment.ApplicationPhysicalPath + "/Log/";
            logFilePath = logFilePath + "Log-" + System.DateTime.Today.ToString("MM-dd-yyyy") + "." + "txt";

            string sLogFormat = DateTime.Now.ToShortDateString().ToString() + " " + DateTime.Now.ToLongTimeString().ToString() + " ==> ";
            StreamWriter sw = new StreamWriter(logFilePath, true);
            sw.WriteLine(sLogFormat + strLog);
            sw.Flush();
            sw.Close();  
        }   

    }
}
